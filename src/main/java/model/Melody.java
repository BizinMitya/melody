package model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Dmitriy on 23.06.2016.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "MELODIES")
public class Melody implements Serializable {
    @Id
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private byte[] melody;

    @ManyToOne
    @JoinColumn(name = "category", nullable = false)
    private Category category;

    @Column(nullable = false)
    private Integer cost;

    public Melody() {

    }

    public Melody(Integer id, String name, String author, byte[] melody, Category category, Integer cost) {
        this.setId(id);
        this.setName(name);
        this.setAuthor(author);
        this.setMelody(melody);
        this.setCategory(category);
        this.setCost(cost);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public byte[] getMelody() {
        return melody;
    }

    public void setMelody(byte[] melody) {
        this.melody = melody;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
