import controller.CategoryController;
import controller.MelodyController;
import controller.impl.CategoryControllerImpl;
import controller.impl.MelodyControllerImpl;
import model.Category;
import model.Melody;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Dmitriy on 29.06.2016.
 */
@WebServlet(name = "SettingServlet")
@MultipartConfig
public class SettingServlet extends HttpServlet {
    private void updateMelody(String melodyId, String name, String author, Part melodyPart) throws IOException, SQLException, NamingException {
        MelodyController melodyController = new MelodyControllerImpl();
        byte[] buffer = new byte[1];
        List<Byte> melody1List = new ArrayList<>();
        InputStream inputStream = melodyPart.getInputStream();
        while (inputStream.read(buffer) != -1) {
            melody1List.add(buffer[0]);
        }
        byte[] melody1 = new byte[melody1List.size()];
        for (int i = 0; i < melody1.length; i++) {
            melody1[i] = melody1List.get(i);
        }
        inputStream.close();
        Melody melody = melodyController.findOne(Integer.valueOf(melodyId));
        melody.setName(name);
        melody.setAuthor(author);
        melody.setMelody(melody1);
        melodyController.update(melody);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        try {
            CategoryController categoryController = new CategoryControllerImpl();
            String categoryName = request.getParameter("category");
            String categoryId = request.getParameter("categoryId");
            Category category = categoryController.findOne(Integer.parseInt(categoryId));
            category.setName(categoryName);
            categoryController.update(category);
            updateMelody(request.getParameter("melody1Id"), request.getParameter("name1"), request.getParameter("author1"), request.getPart("melody1"));
            updateMelody(request.getParameter("melody2Id"), request.getParameter("name2"), request.getParameter("author2"), request.getPart("melody2"));
            updateMelody(request.getParameter("melody3Id"), request.getParameter("name3"), request.getParameter("author3"), request.getPart("melody3"));
            updateMelody(request.getParameter("melody4Id"), request.getParameter("name4"), request.getParameter("author4"), request.getPart("melody4"));
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
