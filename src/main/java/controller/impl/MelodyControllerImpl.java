package controller.impl;

import controller.HibernateUtil;
import controller.MelodyController;
import model.Category;
import model.Melody;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public class MelodyControllerImpl implements MelodyController {
    public static final String FIND_ALL_QUERY = "FROM Melody";
    public static final String FIND_BY_CATEGORY_QUERY = "FROM Melody WHERE category = :category";
    public static final String DELETE = "DELETE FROM Melody WHERE id = :id";
    public static final String UPDATE = "UPDATE Melody SET id = :id, name = :name, author = :author, melody = :melody, category = :category, cost = :cost WHERE id = :id";

    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public Melody findOne(Integer id) throws SQLException, NamingException {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Melody melody = (Melody) session.get(Melody.class, id);
        transaction.commit();
        session.close();
        return melody;
    }

    @Override
    public List<Melody> findAll() {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        List<Melody> arrayList = (List<Melody>) session.createQuery(FIND_ALL_QUERY).list();
        transaction.commit();
        session.close();
        return arrayList;
    }

    @Override
    public List<Melody> findByCategory(Category category) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(FIND_BY_CATEGORY_QUERY);
        query.setParameter("category", category);
        List<Melody> melodies = query.list();
        transaction.commit();
        session.close();
        return melodies;
    }

    @Override
    public void insert(Melody melody) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.save(melody);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Integer id) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(DELETE);
        query.setParameter("id", id);
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

    @Override
    public void update(Melody melody) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(UPDATE);
        query.setParameter("id", melody.getId());
        query.setParameter("name", melody.getName());
        query.setParameter("author", melody.getAuthor());
        query.setParameter("melody", melody.getMelody());
        query.setParameter("category", melody.getCategory());
        query.setParameter("cost", melody.getCost());
        query.executeUpdate();
        transaction.commit();
        session.close();
    }
}
