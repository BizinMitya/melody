package controller.impl;

import controller.CategoryController;
import controller.HibernateUtil;
import model.Category;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public class CategoryControllerImpl implements CategoryController {
    public static final String FIND_ALL_QUERY = "FROM Category";
    public static final String FIND_BY_TOUR_QUERY = "FROM Category WHERE user = :user AND tour = :tour";
    public static final String FIND_BY_USER_QUERY = "FROM Category WHERE user = :user";
    public static final String DELETE = "DELETE FROM Category WHERE id = :id";
    public static final String UPDATE = "UPDATE Category SET id = :id, name = :name, tour = :tour, user = :user WHERE id = :id";

    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public Category findOne(Integer id) throws SQLException, NamingException {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Category category = (Category) session.get(Category.class, id);
        transaction.commit();
        session.close();
        return category;
    }

    @Override
    public List<Category> findAll() {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        List<Category> arrayList = (List<Category>) session.createQuery(FIND_ALL_QUERY).list();
        transaction.commit();
        session.close();
        return arrayList;
    }

    @Override
    public List<Category> findByTour(User user, Integer tour) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(FIND_BY_TOUR_QUERY);
        query.setParameter("user", user);
        query.setParameter("tour", tour);
        List<Category> categories = query.list();
        transaction.commit();
        session.close();
        return categories;
    }

    @Override
    public List<Category> findByUser(User user) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(FIND_BY_USER_QUERY);
        query.setParameter("user", user);
        List<Category> categories = query.list();
        transaction.commit();
        session.close();
        return categories;
    }

    @Override
    public void insert(Category category) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.save(category);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Integer id) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(DELETE);
        query.setParameter("id", id);
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

    @Override
    public void update(Category category) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(UPDATE);
        query.setParameter("id", category.getId());
        query.setParameter("name", category.getName());
        query.setParameter("tour", category.getTour());
        query.setParameter("user", category.getUser());
        query.executeUpdate();
        transaction.commit();
        session.close();
    }
}
