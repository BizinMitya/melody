package controller.impl;

import controller.HibernateUtil;
import controller.UserController;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public class UserControllerImpl implements UserController {
    public static final String FIND_ALL_QUERY = "FROM User";
    public static final String DELETE = "DELETE FROM User WHERE login = :login";
    public static final String UPDATE = "UPDATE User SET login = :login, password = :password, email = :email WHERE login = :login";

    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public User findOne(String login) throws SQLException, NamingException {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        User user = (User) session.get(User.class, login);
        transaction.commit();
        session.close();
        return user;
    }

    @Override
    public List<User> findAll() {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        List<User> arrayList = (List<User>) session.createQuery(FIND_ALL_QUERY).list();
        transaction.commit();
        session.close();
        return arrayList;
    }

    @Override
    public void insert(User user) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(String login) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(DELETE);
        query.setParameter("login", login);
        query.executeUpdate();
        transaction.commit();
        session.close();
    }

    @Override
    public void update(User user) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(UPDATE);
        query.setParameter("login", user.getLogin());
        query.setParameter("password", user.getPassword());
        query.setParameter("email", user.getEmail());
        query.executeUpdate();
        transaction.commit();
        session.close();
    }
}
