package controller;

import model.Category;
import model.User;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public interface CategoryController {
    Category findOne(Integer id) throws SQLException, NamingException;

    List<Category> findAll();

    List<Category> findByTour(User user, Integer tour);

    List<Category> findByUser(User user);

    void insert(Category category);

    void delete(Integer id);

    void update(Category category);
}
