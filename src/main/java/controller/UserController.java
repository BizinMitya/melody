package controller;

import model.User;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public interface UserController {
    User findOne(String login) throws SQLException, NamingException;

    List<User> findAll();

    void insert(User user);

    void delete(String login);

    void update(User user);
}
