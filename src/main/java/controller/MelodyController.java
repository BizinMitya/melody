package controller;

import model.Category;
import model.Melody;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 23.06.2016.
 */
public interface MelodyController {
    Melody findOne(Integer id) throws SQLException, NamingException;

    List<Melody> findAll();

    List<Melody> findByCategory(Category category);

    void insert(Melody melody);

    void delete(Integer id);

    void update(Melody melody);
}
