<%@ page import="controller.UserController" %>
<%@ page import="controller.impl.UserControllerImpl" %>
<%@ page import="model.User" %>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 23.06.2016
  Time: 21:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Забыли пароль?</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.6-dist/css/bootstrap.css">
    <meta charset="utf-8">
    <script>
        function block() {
            forgottenPasswordForm.elements.submit.disabled = true;
            forgottenPasswordForm.elements.submit.value = 'Отправка...';
        }
    </script>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            session.setAttribute("user", null);
            response.sendRedirect("forgotten_password.jsp");
        } else {
            out.print("<h3 class=\"text-center\">Напишите свой логин для сброса пароля Вам на почту</h3>\n" +
                    "<form onsubmit=\"block();\" name=\"forgottenPasswordForm\" class=\"text-center\" method=\"post\"\n" +
                    "      action=\"forgotten_password.jsp\">\n" +
                    "    <div class=\"form-group\">\n" +
                    "        <input title=\"Длина от 3 до 20 символов\"\n" +
                    "               maxlength=\"20\" type=\"text\" size=\"25\" name=\"login\" placeholder=\"Логин\" required\n" +
                    "               pattern=\"[a-zA-Z][a-zA-Z0-9-_\\.]{2,19}\">\n" +
                    "    </div>\n" +
                    "    <div class=\"form-group\">\n" +
                    "        <input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Отправить\">\n" +
                    "    </div>\n" +
                    "<div class=\"form-group\">" +
                    "<a href=\"index.jsp\">На главную</a>" +
                    "</div>" +
                    "</form>");
        }
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        String login = request.getParameter("login");
        UserController userController = new UserControllerImpl();
        User user = userController.findOne(login);
        if (user == null) {
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Вы не зарегистрированы!</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        } else {
            try {
                Session mailSession = (Session) new InitialContext().lookup("java:jboss/mail/mail");
                MimeMessage message = new MimeMessage(mailSession);
                Address[] to = new InternetAddress[]{new InternetAddress(user.getEmail())};
                message.setRecipients(Message.RecipientType.TO, to);
                message.setSubject("Сброс пароля для аккаунта игры \"Угадай мелодию!\"");
                message.setText("Здравствуйте!\n" +
                        "Вы получили это письмо, потому что забыли пароль от своего аккаунта в игре \"Угадай мелодию!\"\n" +
                        "Ваш логин: " + user.getLogin() + "\n" +
                        "Ваш пароль: " + user.getPassword() + "\n" +
                        "Если вы не забывали свой пароль, то просто проигнорируйте это письмо", "UTF-8");
                Transport.send(message);
                out.print("<div class=\"text-center\">\n" +
                        "<h3>На ваш электронный ящик отправлено письмо с паролем!</h3>" +
                        "    <a href=\"index.jsp\">На главную</a>\n" +
                        "</div>");
            } catch (MessagingException | NamingException e) {
                e.printStackTrace();
                out.print("<div class=\"text-center\">\n" +
                        "<h3>Произошла ошибка! Повторите позже</h3>" +
                        "    <a href=\"index.jsp\">На главную</a>\n" +
                        "</div>");
            }
        }
    }
%>
</body>
</html>
