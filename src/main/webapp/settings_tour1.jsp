<%@ page import="controller.CategoryController" %>
<%@ page import="controller.MelodyController" %>
<%@ page import="controller.UserController" %>
<%@ page import="controller.impl.CategoryControllerImpl" %>
<%@ page import="controller.impl.MelodyControllerImpl" %>
<%@ page import="controller.impl.UserControllerImpl" %>
<%@ page import="model.Category" %>
<%@ page import="model.Melody" %>
<%@ page import="model.User" %>
<%@ page import="org.apache.commons.io.FileUtils" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Random" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 26.06.2016
  Time: 11:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Настройки 1 тура</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <style type="text/css">
        .block {
            margin: 10px;
        }
    </style>
    <script>
        function block() {
            categoryForm.elements.submit.disabled = true;
            categoryForm.elements.submit.value = 'Подождите...';
        }
    </script>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    UserController userController = new UserControllerImpl();
    CategoryController categoryController = new CategoryControllerImpl();
    MelodyController melodyController = new MelodyControllerImpl();
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Войдите в профиль!</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        } else {
            out.print("<div class=\"block\"><ul class=\"nav nav-pills\">\n" +
                    "  <li role=\"presentation\"><a href=\"profile.jsp\">Профиль</a></li>\n" +
                    "<li role=\"presentation\" class=\"dropdown\">\n" +
                    "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                    "Игра<span class=\"caret\"></span>\n" +
                    "    </a>\n" +
                    "    <ul class=\"dropdown-menu\">\n" +
                    "<li><a href=\"tour1.jsp\">1 тур</a></li>\n" +
                    "<li><a href=\"tour2.jsp\">2 тур</a></li>\n" +
                    "<li><a href=\"tour3.jsp\">3 тур</a></li>\n" +
                    "<li><a href=\"super_game.jsp\">Супер-игра</a></li>\n" +
                    "    </ul>\n" +
                    "  </li>" +
                    "<li role=\"presentation\" class=\"dropdown active\">\n" +
                    "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                    "Настройки<span class=\"caret\"></span>\n" +
                    "    </a>\n" +
                    "    <ul class=\"dropdown-menu\">\n" +
                    "<li class=\"active\"><a href=\"settings_tour1.jsp\">Настройки 1 тура</a></li>\n" +
                    "<li><a href=\"settings_tour2.jsp\">Настройки 2 тура</a></li>\n" +
                    "<li><a href=\"settings_tour3.jsp\">Настройки 3 тура</a></li>\n" +
                    "<li><a href=\"settings_super_game.jsp\">Настройки Супер-игры</a></li>\n" +
                    "<li class=\"divider\"></li>" +
                    "<li><a href=\"settings_profile.jsp\">Настройки профиля</a></li>\n" +
                    "    </ul>\n" +
                    "  </li>" +
                    "  <li role=\"presentation\"><a href=\"index.jsp\">Выйти</a></li>\n" +
                    "</ul></div>");
            List<Category> categoryList = categoryController.findByTour(user, 1);
            List<Melody> melodyListCategory0 = melodyController.findByCategory(categoryList.get(0));
            List<Melody> melodyListCategory1 = melodyController.findByCategory(categoryList.get(1));
            List<Melody> melodyListCategory2 = melodyController.findByCategory(categoryList.get(2));
            List<Melody> melodyListCategory3 = melodyController.findByCategory(categoryList.get(3));
            out.print("<form onsubmit=\"block(settingsTour1Form)\" class=\"form-inline text-center\" name=\"settingsTour1Form\" action=\"settings_tour1.jsp\" method=\"post\">" +
                    "<table class=\"table table-bordered\">\n" +
                    "    <tr>\n" +
                    "        <th>*</th>\n" +
                    "        <th>Категория</th>\n" +
                    "        <th>10 очков</th>\n" +
                    "        <th>20 очков</th>\n" +
                    "        <th>30 очков</th>\n" +
                    "        <th>40 очков</th>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "        <td><input type=\"radio\" value=\"category1\" name=\"category\" checked=\"checked\">" +
                    "   <input type=\"hidden\" name=\"category1\" value=\"" + categoryList.get(0).getId() + "\"></td>" +
                    "        <td>" + categoryList.get(0).getName() + "</td>\n" +
                    "        <td>" + melodyListCategory0.get(0).getName() + ", " + melodyListCategory0.get(0).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory0.get(1).getName() + ", " + melodyListCategory0.get(1).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory0.get(2).getName() + ", " + melodyListCategory0.get(2).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory0.get(3).getName() + ", " + melodyListCategory0.get(3).getAuthor() + "</td>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "        <td><input type=\"radio\" value=\"category2\" name=\"category\">" +
                    "<input type=\"hidden\" name=\"category2\" value=\"" + categoryList.get(1).getId() + "\"></td>" +
                    "        <td>" + categoryList.get(1).getName() + "</td>\n" +
                    "        <td>" + melodyListCategory1.get(0).getName() + ", " + melodyListCategory1.get(0).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory1.get(1).getName() + ", " + melodyListCategory1.get(1).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory1.get(2).getName() + ", " + melodyListCategory1.get(2).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory1.get(3).getName() + ", " + melodyListCategory1.get(3).getAuthor() + "</td>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "        <td><input type=\"radio\" value=\"category3\" name=\"category\">" +
                    "<input type=\"hidden\" name=\"category3\" value=\"" + categoryList.get(2).getId() + "\"></td>" +
                    "        <td>" + categoryList.get(2).getName() + "</td>\n" +
                    "        <td>" + melodyListCategory2.get(0).getName() + ", " + melodyListCategory2.get(0).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory2.get(1).getName() + ", " + melodyListCategory2.get(1).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory2.get(2).getName() + ", " + melodyListCategory2.get(2).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory2.get(3).getName() + ", " + melodyListCategory2.get(3).getAuthor() + "</td>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "        <td><input type=\"radio\" value=\"category4\" name=\"category\">" +
                    "<input type=\"hidden\" name=\"category4\" value=\"" + categoryList.get(3).getId() + "\"></td>" +
                    "        <td>" + categoryList.get(3).getName() + "</td>\n" +
                    "        <td>" + melodyListCategory3.get(0).getName() + ", " + melodyListCategory3.get(0).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory3.get(1).getName() + ", " + melodyListCategory3.get(1).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory3.get(2).getName() + ", " + melodyListCategory3.get(2).getAuthor() + "</td>\n" +
                    "        <td>" + melodyListCategory3.get(3).getName() + ", " + melodyListCategory3.get(3).getAuthor() + "</td>\n" +
                    "    </tr>\n" +
                    "</table>" +
                    "<input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Редактировать\">" +
                    "</form>");
        }
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        User user = (User) session.getAttribute("user");
        Random random = new Random();
        out.print("<div class=\"block\"><ul class=\"nav nav-pills\">\n" +
                "  <li role=\"presentation\"><a href=\"profile.jsp\">Профиль</a></li>\n" +
                "<li role=\"presentation\" class=\"dropdown\">\n" +
                "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                "Игра<span class=\"caret\"></span>\n" +
                "    </a>\n" +
                "    <ul class=\"dropdown-menu\">\n" +
                "<li><a href=\"tour1.jsp\">1 тур</a></li>\n" +
                "<li><a href=\"tour2.jsp\">2 тур</a></li>\n" +
                "<li><a href=\"tour3.jsp\">3 тур</a></li>\n" +
                "<li><a href=\"super_game.jsp\">Супер-игра</a></li>\n" +
                "    </ul>\n" +
                "  </li>" +
                "<li role=\"presentation\" class=\"dropdown active\">\n" +
                "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                "Настройки<span class=\"caret\"></span>\n" +
                "    </a>\n" +
                "    <ul class=\"dropdown-menu\">\n" +
                "<li class=\"active\"><a href=\"settings_tour1.jsp\">Настройки 1 тура</a></li>\n" +
                "<li><a href=\"settings_tour2.jsp\">Настройки 2 тура</a></li>\n" +
                "<li><a href=\"settings_tour3.jsp\">Настройки 3 тура</a></li>\n" +
                "<li><a href=\"settings_super_game.jsp\">Настройки Супер-игры</a></li>\n" +
                "<li class=\"divider\"></li>" +
                "<li><a href=\"settings_profile.jsp\">Настройки профиля</a></li>\n" +
                "    </ul>\n" +
                "  </li>" +
                "  <li role=\"presentation\"><a href=\"index.jsp\">Выйти</a></li>\n" +
                "</ul></div>");
        //TODO удалять файлы музыки!!!
        try {
            File audio = new File("audio");
            if (!audio.exists()) {
                boolean res = audio.mkdir();
                if (res) {
                    System.out.println("Папка audio создана!");
                } else {
                    System.out.println("Папка audio не создана!");
                }
            }
            File login = new File("audio/" + user.getLogin());
            if (login.exists()) {
                FileUtils.deleteDirectory(new File("audio/" + user.getLogin()));
                System.out.println("Папка " + user.getLogin() + " удалена!");
            }
            boolean res = login.mkdir();
            if (res) {
                System.out.println("Папка " + user.getLogin() + " создана!");
            } else {
                System.out.println("Папка " + user.getLogin() + " не создана!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Папка " + user.getLogin() + " не удалена!");
        }
        String categoryNumber = request.getParameter("category");
        String categoryId = request.getParameter(categoryNumber);
        Category category = categoryController.findOne(Integer.parseInt(categoryId));
        List<Melody> melodies = melodyController.findByCategory(category);
        out.print("<form class=\"form-inline\" enctype=\"multipart/form-data\" onsubmit=\"block();\" name=\"categoryForm\" action=\"SettingServlet\" method=\"post\">");
        out.print("<div class=\"text-center\">" +
                "<input title=\"Название категории\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"category\" placeholder=\"Категория\" value=\"" + category.getName() + "\" required>" +
                "<input type=\"hidden\" name=\"categoryId\" value=\"" + category.getId() + "\">" +
                "</div><br>");
        out.print("<div class=\"form-group\">" +
                "Цена мелодии: " + melodies.get(0).getCost() + "&nbsp;" +
                "<input class=\"form-control\" title=\"Название песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"name1\" placeholder=\"Название песни\" value=\"" + melodies.get(0).getName() + "\" required>&nbsp;" +
                "<input class=\"form-control\" title=\"Автор песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"author1\" placeholder=\"Автор песни\" value=\"" + melodies.get(0).getAuthor() + "\" required>&nbsp;");
        if (melodies.get(0).getMelody().length != 1) {
            byte[] melody = melodies.get(0).getMelody();
            int nameOfFile = random.nextInt();
            File file = new File("audio/" + user.getLogin() + "/" + String.valueOf(nameOfFile));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(melody);
            fileOutputStream.close();
            out.print("<audio controls>\n" +
                    " <source src=\"" + "audio/" + user.getLogin() + "/" + nameOfFile + ".mp3\">\n" +
                    " </audio>");
        }
        out.print("<input class=\"form-control\" type=\"file\" name=\"melody1\" accept=\"audio/mpeg\" required>" +
                "<input type=\"hidden\" name=\"melody1Id\" value=\"" + melodies.get(0).getId() + "\">");
        out.print("</div><br><br>");
        out.print("<div class=\"form-group\">" +
                "Цена мелодии: " + melodies.get(1).getCost() + "&nbsp;" +
                "<input class=\"form-control\" title=\"Название песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"name2\" placeholder=\"Название песни\" value=\"" + melodies.get(1).getName() + "\" required>&nbsp;" +
                "<input class=\"form-control\" title=\"Автор песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"author2\" placeholder=\"Автор песни\" value=\"" + melodies.get(1).getAuthor() + "\" required>&nbsp;");
        if (melodies.get(1).getMelody().length != 1) {
            byte[] melody = melodies.get(1).getMelody();
            int nameOfFile = random.nextInt();
            File file = new File("audio/" + user.getLogin() + "/" + String.valueOf(nameOfFile));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(melody);
            fileOutputStream.close();
            out.print("<audio controls>\n" +
                    " <source src=\"" + "audio/" + user.getLogin() + "/" + nameOfFile + ".mp3\">\n" +
                    " </audio>");
        }
        out.print("<input class=\"form-control\" type=\"file\" name=\"melody2\" accept=\"audio/mpeg\" required>" +
                "<input type=\"hidden\" name=\"melody2Id\" value=\"" + melodies.get(1).getId() + "\">");
        out.print("</div><br><br>");
        out.print("<div class=\"form-group\">" +
                "Цена мелодии: " + melodies.get(2).getCost() + "&nbsp;" +
                "<input class=\"form-control\" title=\"Название песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"name3\" placeholder=\"Название песни\" value=\"" + melodies.get(2).getName() + "\" required>&nbsp;" +
                "<input class=\"form-control\" title=\"Автор песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"author3\" placeholder=\"Автор песни\" value=\"" + melodies.get(2).getAuthor() + "\" required>&nbsp;");
        if (melodies.get(2).getMelody().length != 1) {
            byte[] melody = melodies.get(2).getMelody();
            int nameOfFile = random.nextInt();
            File file = new File("audio/" + user.getLogin() + "/" + String.valueOf(nameOfFile));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(melody);
            fileOutputStream.close();
            out.print("<audio controls>\n" +
                    " <source src=\"" + "audio/" + user.getLogin() + "/" + nameOfFile + ".mp3\">\n" +
                    " </audio>");
        }
        out.print("<input class=\"form-control\" type=\"file\" name=\"melody3\" accept=\"audio/mpeg\" required>" +
                "<input type=\"hidden\" name=\"melody3Id\" value=\"" + melodies.get(2).getId() + "\">");
        out.print("</div><br><br>");
        out.print("<div class=\"form-group\">" +
                "Цена мелодии: " + melodies.get(3).getCost() + "&nbsp;" +
                "<input class=\"form-control\" title=\"Название песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"name4\" placeholder=\"Название песни\" value=\"" + melodies.get(3).getName() + "\" required>&nbsp;" +
                "<input class=\"form-control\" title=\"Автор песни\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"author4\" placeholder=\"Автор песни\" value=\"" + melodies.get(3).getAuthor() + "\" required>&nbsp;");
        if (melodies.get(3).getMelody().length != 1) {
            byte[] melody = melodies.get(3).getMelody();
            int nameOfFile = random.nextInt();
            File file = new File("audio/" + user.getLogin() + "/" + String.valueOf(nameOfFile));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(melody);
            fileOutputStream.close();
            out.print("<audio controls>\n" +
                    " <source src=\"" + "audio/" + user.getLogin() + "/" + nameOfFile + ".mp3\">\n" +
                    " </audio>");
        }
        out.print("<input class=\"form-control\" type=\"file\" name=\"melody4\" accept=\"audio/mpeg\" required>" +
                "<input type=\"hidden\" name=\"melody4Id\" value=\"" + melodies.get(3).getId() + "\">");
        out.print("</div><br><br>");
        out.print("<div class=\"text-center\"><input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Изменить\"></div>");
        out.print("</form>");
    }
%>
</body>
</html>
