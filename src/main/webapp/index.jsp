<%@ page import="model.User" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 20.06.2016
  Time: 11:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Угадай мелодию!</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.6-dist/css/bootstrap.css">
    <meta charset="utf-8">
    <style type="text/css">
        #footer {
            position: fixed; /* Фиксированное положение */
            left: 44%;
            bottom: 0; /* Левый нижний угол */
            padding: 10px; /* Поля вокруг текста */
            width: 100%; /* Ширина слоя */
        }
    </style>
    <script>
        function block() {
            loginForm.elements.submit.disabled = true;
            loginForm.elements.submit.value = 'Вход...';
        }
    </script>
</head>
<body> <!--background="images/titleImage.jpg" class="img-responsive"-->
<%
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            session.setAttribute("user", null);
            response.sendRedirect("index.jsp");
        } else {
            out.print("<h1 class=\"text-center\">Добро пожаловать в игру \"Угадай мелодию!\"</h1>\n" +
                    "<h3 class=\"text-center\">Для начала нужно авторизоваться</h3>\n" +
                    "<form name=\"loginForm\" onsubmit=\"block()\" class=\"form-inline text-center\" method=\"post\" action=\"profile.jsp\">\n" +
                    "    <input title=\"Длина от 3 до 20 символов\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"login\" placeholder=\"Логин\" required\n" +
                    "           pattern=\"[a-zA-Z][a-zA-Z0-9-_\\.]{2,19}\">\n" +
                    "    <input title=\"Требования:\n" +
                    "    1)длина от 6 до 20 символов;\n" +
                    "    2)только латинские буквы;\n" +
                    "    3)хотя бы одна цифра;\n" +
                    "    4)хотя бы одна строчная буква;\n" +
                    "    5)хотя бы одна прописная буква;\n" +
                    "    6)разрешены тире и подчёркивание\""+
                    "           maxlength=\"20\" type=\"password\" size=\"25\" name=\"password\" placeholder=\"Пароль\" required\n" +
                    "           pattern=\"(?=[-_a-zA-Z0-9]*?[A-Z])(?=[-_a-zA-Z0-9]*?[a-z])(?=[-_a-zA-Z0-9]*?[0-9])[-_a-zA-Z0-9]{6,20}\">\n" +
                    "    <input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Войти\">\n" +
                    "</form>\n" +
                    "<div class=\"text-center\">\n" +
                    "    <a href=\"registration.jsp\">Зарегистрироваться</a>\n" +
                    "</div>\n" +
                    "<div class=\"text-center\">\n" +
                    "    <a href=\"forgotten_password.jsp\">Забыли пароль?</a>\n" +
                    "</div>\n" +
                    "<div id=\"footer\">\n" +
                    "    <a href=\"feedback.jsp\">Написать разработчику</a>\n" +
                    "</div>");
        }
    }
%>
</body>
</html>
