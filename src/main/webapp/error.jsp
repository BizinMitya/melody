<%@ page import="model.User" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 22.06.2016
  Time: 13:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>Ошибка</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.6-dist/css/bootstrap.css">
    <meta charset="utf-8">
</head>
<body>
<%
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            session.setAttribute("user", null);
            response.sendRedirect("error.jsp");
        } else {
            if (exception != null) {
                out.print("<h3 class=\"text-center\">" + exception.getMessage()  + "</h3>");
                exception.printStackTrace();
            }
        }
    }
%>
<div class="text-center">
    <a href="index.jsp">На главную</a>
</div>
</body>
</html>
