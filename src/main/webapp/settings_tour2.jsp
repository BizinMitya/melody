<%@ page import="model.User" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 26.06.2016
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Настройки 2 тура</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <style type="text/css">
        .block{
            margin: 10px;
        }
    </style>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Войдите в профиль!</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        } else {
            out.print("<div class=\"block\"><ul class=\"nav nav-pills\">\n" +
                    "  <li role=\"presentation\"><a href=\"profile.jsp\">Профиль</a></li>\n" +
                    "<li role=\"presentation\" class=\"dropdown\">\n" +
                    "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                    "Игра<span class=\"caret\"></span>\n" +
                    "    </a>\n" +
                    "    <ul class=\"dropdown-menu\">\n" +
                    "<li><a href=\"tour1.jsp\">1 тур</a></li>\n" +
                    "<li><a href=\"tour2.jsp\">2 тур</a></li>\n" +
                    "<li><a href=\"tour3.jsp\">3 тур</a></li>\n" +
                    "<li><a href=\"super_game.jsp\">Супер-игра</a></li>\n" +
                    "    </ul>\n" +
                    "  </li>" +
                    "<li role=\"presentation\" class=\"dropdown active\">\n" +
                    "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                    "Настройки<span class=\"caret\"></span>\n" +
                    "    </a>\n" +
                    "    <ul class=\"dropdown-menu\">\n" +
                    "<li><a href=\"settings_tour1.jsp\">Настройки 1 тура</a></li>\n" +
                    "<li class=\"active\"><a href=\"settings_tour2.jsp\">Настройки 2 тура</a></li>\n" +
                    "<li><a href=\"settings_tour3.jsp\">Настройки 3 тура</a></li>\n" +
                    "<li><a href=\"settings_super_game.jsp\">Настройки Супер-игры</a></li>\n" +
                    "<li class=\"divider\"></li>" +
                    "<li><a href=\"settings_profile.jsp\">Настройки профиля</a></li>\n" +
                    "    </ul>\n" +
                    "  </li>" +
                    "  <li role=\"presentation\"><a href=\"index.jsp\">Выйти</a></li>\n" +
                    "</ul></div>");
        }
    }
%>
<table class="table table-bordered">
    <tr>
        <th>Категория</th><th>10 очков</th><th>20 очков</th><th>30 очков</th><th>40 очков</th>
    </tr>
    <tr>
        <td>Категория1</td><td>Название1,автор1</td><td>Название2,автор2</td><td>Название3,автор3</td><td>Название4,автор4</td>
    </tr>
    <tr>
        <td>Категория2</td><td>Название5,автор5</td><td>Название6,автор6</td><td>Название7,автор7</td><td>Название8,автор8</td>
    </tr>
    <tr>
        <td>Категория3</td><td>Название9,автор9</td><td>Название10,автор10</td><td>Название11,автор11</td><td>Название12,автор12</td>
    </tr>
    <tr>
        <td>Категория4</td><td>Название13,автор13</td><td>Название14,автор14</td><td>Название15,автор15</td><td>Название16,автор16</td>
    </tr>
</table>
</body>
</html>
