<%@ page import="model.User" %>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 24.06.2016
  Time: 22:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Feedback</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.6-dist/css/bootstrap.css">
    <meta charset="utf-8">
    <script>
        function block() {
            feedbackForm.elements.submit.disabled = true;
            feedbackForm.elements.submit.value = 'Отправка...'
        }
    </script>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    if (request.getMethod().equalsIgnoreCase("get")) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            session.setAttribute("user", null);
            response.sendRedirect("feedback.jsp");
        } else {
            out.print("<h3 class=\"text-center\">Напишите в поле ниже свои замечания и предложения по поводу игры</h3>\n" +
                    "<form onsubmit=\"block();\" name=\"feedbackForm\" class=\"text-center\" method=\"post\" action=\"feedback.jsp\">\n" +
                    "    <div class=\"form-group\">\n" +
                    "        <textarea style=\"resize: none;\" rows=\"20\" cols=\"100\" name=\"letter\" class=\"text-area\" placeholder=\"Письмо разработчику\"\n" +
                    "                  required></textarea>\n" +
                    "    </div>\n" +
                    "    <div class=\"form-group\">\n" +
                    "        <input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Отправить\">\n" +
                    "    </div>\n" +
                    "<div class=\"form-group\">\n" +
                    "<a href=\"index.jsp\">На главную</a>\n" +
                    "</div>\n" +
                    "</form>");
        }
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        try {
            String letter = request.getParameter("letter");
            Session mailSession = (Session) new InitialContext().lookup("java:jboss/mail/mail");
            MimeMessage message = new MimeMessage(mailSession);
            Address[] to = new InternetAddress[]{new InternetAddress("bizin.mitya@mail.ru")};
            message.setRecipients(Message.RecipientType.TO, to);
            message.setSubject("Feedback от пользователя игры \"Угадай мелодию!\"");
            message.setText(letter, "UTF-8");
            Transport.send(message);
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Ваше письмо отправлено!</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        } catch (MessagingException | NamingException e) {
            e.printStackTrace();
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Произошла ошибка! Повторите позже</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        }
    }
%>
</body>
</html>
