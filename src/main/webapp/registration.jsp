<%@ page import="controller.CategoryController" %>
<%@ page import="controller.MelodyController" %>
<%@ page import="controller.UserController" %>
<%@ page import="controller.impl.CategoryControllerImpl" %>
<%@ page import="controller.impl.MelodyControllerImpl" %>
<%@ page import="controller.impl.UserControllerImpl" %>
<%@ page import="model.Category" %>
<%@ page import="model.Melody" %>
<%@ page import="model.User" %>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Random" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 20.06.2016
  Time: 12:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-3.3.6-dist/css/bootstrap.css">
    <meta charset="utf-8">
    <script>
        function block() {
            registrationForm.elements.submit.disabled = true;
            registrationForm.elements.submit.value = 'Регистрация...';
        }
    </script>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    UserController userController = new UserControllerImpl();
    CategoryController categoryController = new CategoryControllerImpl();
    MelodyController melodyController = new MelodyControllerImpl();
    if (request.getMethod().equalsIgnoreCase("get")) {
        String code = request.getParameter("code");
        if (code != null) {
            if (session.getAttribute("user") != null) {
                session.setAttribute("user", null);
                response.sendRedirect("registration.jsp?code=" + code);
            } else {
                User user = (User) session.getAttribute(code);
                if (user != null) {
                    userController.insert(user);
                    for (int i = 0; i < 12; i++) {
                        categoryController.insert(new Category(0, "Категория " + (i + 1), i / 4 + 1, user));
                    }
                    for (int i = 0; i < 3; i++) {
                        List<Category> categories = categoryController.findByTour(user, i + 1);
                        for (int j = 0; j < 4; j++) {
                            melodyController.insert(new Melody(0, "Без названия", "Без автора", new byte[]{0}, categories.get(j), 10));
                            melodyController.insert(new Melody(0, "Без названия", "Без автора", new byte[]{0}, categories.get(j), 20));
                            melodyController.insert(new Melody(0, "Без названия", "Без автора", new byte[]{0}, categories.get(j), 30));
                            melodyController.insert(new Melody(0, "Без названия", "Без автора", new byte[]{0}, categories.get(j), 40));
                        }
                    }
                    out.print("<div class=\"text-center\">\n" +
                            "<h3>Вы зарегистрированы!</h3>" +
                            "    <a href=\"index.jsp\">На главную</a>\n" +
                            "</div>");
                    session.setAttribute(code, null);
                } else {
                    out.print("<div class=\"text-center\">\n" +
                            "<h3>Вы уже зарегистрированы!</h3>" +
                            "    <a href=\"index.jsp\">На главную</a>\n" +
                            "</div>");
                }
            }
        } else {
            User user = (User) session.getAttribute("user");
            if (user != null) {
                session.setAttribute("user", null);
                response.sendRedirect("registration.jsp");
            } else {
                out.print("<form onsubmit=\"block();\" name=\"registrationForm\" class=\"text-center\" method=\"post\" action=\"registration.jsp\">\n" +
                        "<div class=\"form-group\">" +
                        "    <input type=\"email\" title=\"Введите ваш корректный адрес электронной почты\" size=\"25\" name=\"email\" placeholder=\"Email\" required pattern=\"\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,6})+\">\n" +
                        "</div>" +
                        "<div class=\"form-group\">" +
                        "    <input title=\"Длина от 3 до 20 символов\" maxlength=\"20\" type=\"text\" size=\"25\" name=\"login\" placeholder=\"Логин\" required pattern=\"[a-zA-Z][a-zA-Z0-9-_\\.]{2,19}\">\n" +
                        "</div>" +
                        "<div class=\"form-group\">" +
                        "    <input title=\"Требования:\n1)длина от 6 до 20 символов;\n2)только латинские буквы;\n3)хотя бы одна цифра;\n4)хотя бы одна строчная буква;\n5)хотя бы одна прописная буква;\n6)разрешены тире и подчёркивание\" maxlength=\"20\" type=\"password\" size=\"25\" name=\"password\" placeholder=\"Пароль\" required pattern=\"(?=[-_a-zA-Z0-9]*?[A-Z])(?=[-_a-zA-Z0-9]*?[a-z])(?=[-_a-zA-Z0-9]*?[0-9])[-_a-zA-Z0-9]{6,20}\">\n" +
                        "</div>" +
                        "    <input name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"Зарегистрироваться\">\n" +
                        "<div class=\"form-group\">" +
                        "<a href=\"index.jsp\">На главную</a>" +
                        "</div>" +
                        "</form>");
            }
        }
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        try {
            Random random = new Random();
            int code = random.nextInt();
            Session mailSession = (Session) new InitialContext().lookup("java:jboss/mail/mail");
            String email = request.getParameter("email");
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            if (userController.findOne(login) == null) {
                List<User> userList = userController.findAll();
                boolean flag = false;
                for (User user : userList) {
                    if (user.getEmail().equals(email)) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    out.print("<div class=\"text-center\">\n" +
                            "<h3>На этот электронный адрес уже зарегистрирован логин!</h3>" +
                            "    <a href=\"registration.jsp\">Регистрация</a>\n" +
                            "</div>");
                } else {
                    session.setAttribute(String.valueOf(code), new User(login, password, email));
                    MimeMessage message = new MimeMessage(mailSession);
                    Address[] to = new InternetAddress[]{new InternetAddress(email)};
                    message.setRecipients(Message.RecipientType.TO, to);
                    message.setSubject("Регистрация в игре \"Угадай мелодию!\"");
                    message.setText("Здравствуйте!\n" +
                            "Вы получили это письмо, потому что хотели зарегистрироваться в онлайн-игре \"Угадай мелодию!\"\n" +
                            "Ваш логин: " + login + "\n" +
                            "Ваш пароль: " + password + "\n" +
                            "Для того, чтобы подтвердить регистрацию, перейдите, пожалуйста, по ссылке http://melody-javanet.rhcloud.com/registration.jsp?code=" + code + "\n" +
                            "Если вы не регистрировались в игре и понятия не имеете, о чём идёт речь, просто проигнорируйте это письмо и переход по ссылке", "UTF-8");
                    Transport.send(message);
                    out.print("<div class=\"text-center\">\n" +
                            "<h3>Вам было отправлено письмо на почту для подтверждения регистрации</h3>" +
                            "    <a href=\"index.jsp\">На главную</a>\n" +
                            "</div>");
                }
            } else {
                out.print("<div class=\"text-center\">\n" +
                        "<h3>Этот логин уже используется! Попробуйте другой</h3>" +
                        "    <a href=\"registration.jsp\">Регистрация</a>\n" +
                        "</div>");
            }
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new Exception("Неправильный адрес email!");
        } catch (NamingException e) {
            e.printStackTrace();
            out.print("<div class=\"text-center\">\n" +
                    "<h3>Произошла ошибка! Повторите позже</h3>" +
                    "    <a href=\"index.jsp\">На главную</a>\n" +
                    "</div>");
        }
    }
%>
</body>
</html>
