-- MySQL Script generated by MySQL Workbench
-- 06/28/16 15:51:18
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema melody
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema melody
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `melody` DEFAULT CHARACTER SET utf8 ;
USE `melody` ;

-- -----------------------------------------------------
-- Table `melody`.`USERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `melody`.`USERS` (
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`login`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `melody`.`CATEGORIES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `melody`.`CATEGORIES` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `tour` VARCHAR(45) NOT NULL,
  `user` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CATEGORIES_USERS_idx` (`user` ASC),
  CONSTRAINT `fk_CATEGORIES_USERS`
    FOREIGN KEY (`user`)
    REFERENCES `melody`.`USERS` (`login`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `melody`.`MELODIES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `melody`.`MELODIES` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `melody` BLOB NOT NULL,
  `cost` INT NOT NULL,
  `category` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MELODIES_CATEGORIES1_idx` (`category` ASC),
  CONSTRAINT `fk_MELODIES_CATEGORIES1`
    FOREIGN KEY (`category`)
    REFERENCES `melody`.`CATEGORIES` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
